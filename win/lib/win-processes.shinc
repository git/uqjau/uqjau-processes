#!/bin/false -meant2bsourced

# -------------------------------------------------------------------- 
# windows process related
#   Meant for use in interactive shell; source this as part of
#   your shell login.
# -------------------------------------------------------------------- 

# ====================================================================
# Copyright (c) 2010 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# -- Software License --
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---

_pslist()
{
  # -------------------------------------------------------------------- 
  # Synopsis: Windows pslist task mgr like behavior w/%CPU 
  # Ex
  #  _pslist idle
  # -------------------------------------------------------------------- 
  local pid_or_name=${1:-}
  if ! test -n "$pid_or_name";then 
    echo $FUNCNAME:want name pid or name>&2
    return 1
  fi

  # bug? in PsList 1.26 => 1st run is bad data, so "-s 2"
  pslist -s 2 "${pid_or_name}" |
  perl -lne '
    $instance++ if (m{^Name\b});
    exit 0 if (m{^process \w+ was not}); # another pslist oddity?
    print if ($instance >=2);'
}

psl() { 
  # -------------------------------------------------------------------- 
  # Synopsis: wrapper for psloglist, converting output to UNIX text.
  # -------------------------------------------------------------------- 
  psloglist "$@"|d2u; 
}

cygpidlu()
{
  # -------------------------------------------------------------------- 
  # Synopsis: cygwin to windows pid. grep for a cygwin PID in [non /proc]
  # cygwin ps table - to show winpid
  # -------------------------------------------------------------------- 
  : cygwin to windows pid
  : cygpidlu: grep for a cygwin PID in [non /proc] cygwin ps table - to show winpid
  local cygpid 
  local just_started
  local ps_out

  ps_out=$(command ps -el)
    #snapshot cygwin processes
  for cygpid
  do
    let ++just_started
    echo "$ps_out"|
    awk '
      NR == 1 && '"$just_started"' == 1 { print; next; }

      NR > 1 { origline = $0; sub (/^[A-Z]/," ",$0); }
        # the sub cleans up lines like:
        #I    2648    5680    2648       4236    0 17557   Nov  6 /usr/bin/bash

      $1 == '"$cygpid"' { print origline; OK=1; }

      END {
        if ( OK != 1 ) {
           print "ERROR: pid not found: '"$cygpid"'"  > "/dev/stderr"
        }
      }
    '
    #TBD - re-write 'for' block in perl w/associate array so can 'grok' $ps_out just once
  done
}
# cd /proc; cygpidlu [0-9]*

winpidlu()
{
  # -------------------------------------------------------------------- 
  # Synopsis: grep for a windows PID in [non /proc] cygwin ps table
  # windows to cygwin pid
  # -------------------------------------------------------------------- 
  : windows to cygwin pid 
  : winpidlu: grep for a windows PID in [non /proc] cygwin ps table
  local winpid
  local just_started

  for winpid
  do
    let ++just_started
    command ps -elW|
    awk '
      NR == 1 && '"$just_started"' == 1 { print; next; }
        # print the header exactly once

      NR > 1 { origline = $0; sub (/^[A-Z]/," ",$0); }
        # the sub cleans up lines like:
        #I    2648    5680    2648       4236    0 17557   Nov  6 /usr/bin/bash

      $4 == '"$winpid"' { print origline; OK=1; }
        # print the matching line

      END {
        if ( OK != 1 ) {
           print "ERROR: pid not found: '"$winpid"'"  > "/dev/stderr"
        }
      }
    ' 
  done
}

_top()
{
  # -------------------------------------------------------------------- 
  # Synopsis: wrapper for windows pslist
  # -------------------------------------------------------------------- 
  pslist -s ${1:-2} | perl -ne '
    next if $. == 1;
      # skip first run, since it usually does not correct CPU% (bug in PsList 1.26)
      # 1st pslist line would match regex on next code line below

    $trig = m{Process information for } if (!$after1streport);
      #regex flips true then never used again

    $after1streport=1 if $trig;

    if ( ($after1streport) and (!$zero_percent_CPU) )
    {
      print "\n" if ($zero_percent_CPU = m{^\w+\s+\d+\s+0\s+});
        #only triggers on 1st process using under 1% CPU

      print unless ($zero_percent_CPU or m{^\s*$});
    }
    else
    { if(m{Process information for }) {$zero_percent_CPU=0;print} }
  '
}

pslists()
{

# -------------------------------------------------------------------- 
# Synopsis: wrapper for windows pslist, about same as "_top" but
# shows *all* processes
# -------------------------------------------------------------------- 

# task manager like output, shows %CPU
pslist -s ${1:-4} |
  perl -ne '
   # purpose: skip first run, since it usually does not show Idle %
   next if $. == 1; 
   $y = m{Process information for } if (!$x);
   $x=1 if $y; 
   print if $x;
  ' |less
}
